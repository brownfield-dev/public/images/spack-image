FROM ubuntu:20.04

USER root

RUN apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y \
    # Development utilities
    git \
    bash \
    curl \
    htop \
    man \
    vim \
    ssh \
    sudo \
    lsb-release \
    ca-certificates \
    locales \
    gnupg \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    nano \
    unzip

RUN git clone https://github.com/spack/spack ~/spack && \
    cd ~/spack && \
    git checkout releases/v0.15 && \
    source share/spack/setup-env.sh


RUN adduser --gecos '' --disabled-password coder && \
  echo "coder ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nopasswd
USER coder
